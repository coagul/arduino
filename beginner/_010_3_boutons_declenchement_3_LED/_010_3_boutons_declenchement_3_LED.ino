/*

 Déclencher l'allumage de 3 LED à l'aide de 3 interrupteurs
 Apprentissage du binaire en observant le nombre de combinaisons possibles
 
 */

// Initialisation des constantes :
const int bouton1 = 4;     // Numéro de la broche à laquelle est connecté le bouton poussoir 1
const int bouton2 = 3;     // Numéro de la broche à laquelle est connecté le bouton poussoir 2
const int bouton3 = 2;     // Numéro de la broche à laquelle est connecté le bouton poussoir 3

const int ledVerte =  13;      // Numéro de la broche à laquelle est connectée la LED verte
const int ledJaune =  12;      // Numéro de la broche à laquelle est connectée la LED jaune
const int ledRouge =  11;      // Numéro de la broche à laquelle est connectée la LED rouge

// Déclaration des variables :
int etatBouton1 = 0;         // variable qui sera utilisée pour stocker l'état du bouton 1
int etatBouton2 = 0;         // variable qui sera utilisée pour stocker l'état du bouton 2
int etatBouton3 = 0;         // variable qui sera utilisée pour stocker l'état du bouton 3

// le code dans cette fonction est exécuté une fois au début
void setup() {
  // indique que la broche ledVerte, ledJaune et ledRouge sont des sorties :
  pinMode(ledVerte, OUTPUT);
  pinMode(ledJaune, OUTPUT);      
  pinMode(ledRouge, OUTPUT);        
  // indique que la broche bouton est une entrée :
  pinMode(bouton1, INPUT);     
  pinMode(bouton2, INPUT);     
  pinMode(bouton3, INPUT);     
}

// le code dans cette fonction est exécuté en boucle
void loop(){
  // lit l'état du bouton et stocke le résultat dans etatBouton
  etatBouton1 = digitalRead(bouton1);
  etatBouton2 = digitalRead(bouton2);
  etatBouton3 = digitalRead(bouton3);
  // Si etatBouton1 est à 5V (HIGH) c'est que le bouton est appuyé
  if (etatBouton1 == HIGH) {     
    digitalWrite(ledVerte, HIGH);   // on éteind la LED verte
    delay(500);                   // on laisse allumée la led jaune 1/2 seconde
  }
  else if (etatBouton2 == HIGH) {     
    digitalWrite(ledJaune, HIGH);  // on allume la LED jaune
    delay(500);                   // on laisse allumée la led jaune 1/2 seconde 
  }
  else if (etatBouton3 == HIGH) {     
    digitalWrite(ledRouge, HIGH);  // on allume la LED jaune
    delay(500);                   // on laisse allumée la led jaune 1/2 seconde 
  }
  else {
    // sinon on éteind les LED
    digitalWrite(ledVerte, LOW); 
    digitalWrite(ledJaune, LOW);
    digitalWrite(ledRouge, LOW);
  }
}

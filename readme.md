# Arduino & COAGUL

Vous trouverez ici les fichiers sources des montages Arduino réalisés dans le cadre de notre hackerspace et fablab L'abscisse basé à Dijon.

https://fablab.coagul.org

## Les montages documentés

Les montages sont documentés dans la catégorie Arduino de notre wiki.

https://fablab.coagul.org/Cat%C3%A9gorie:Arduino

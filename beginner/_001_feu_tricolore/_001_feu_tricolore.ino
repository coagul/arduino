/*
  Clignotement à tour de rôle d'un feu tricolore
  
  Orange allumée pendant 1 seconde (la LED orange devra être la première LED à s'allumer)
  Rouge allumée pendant 3 secondes (comme dans la vraie vie, orange devra s'éteindre et la LED rouge s'allume en second)
  Verte allumée pendant 3 secondes (après rouge, on passe au vert)
 
  Cette exemple est dans le domaine public

  Câblage :

  La broche 13 est reliée à la LED verte (LED → la résistance → GND)
  La broche 12 est reliée à la LED orange (LED → la résistance → GND)
  La broche 11 est reliée à la LED verte (LED → la résistance → GND)

*/

// déclare 3 variables nommées verte, orange, rouge de type entier, permet de faire référence aux différentes broches de sortie dans le programme
int verte = 13;
int orange = 12;
int rouge = 11;

// le setup regroupe les instructions qui seront exécutées au démarrage du programme y compris quand on presse le bouton reset
void setup() {                
  // on initialise les 3 broches de l'Arduino en tant que sortie
  pinMode(verte, OUTPUT);
  pinMode(orange, OUTPUT);
  pinMode(rouge, OUTPUT);
}

// contient les instructions que l'on souhaite voir exécutées encore et encore tant que l'Arduino est alimenté
void loop() {
  digitalWrite(orange, HIGH);   // allumer la LED orange (HIGH est le niveau de voltage, état haut)
  delay(1000);                  // 1000 millisecondes, attendre une seconde
  digitalWrite(orange, LOW);    // éteindre la LED orange (LOW est le niveau de voltage, état bas)
  digitalWrite(rouge, HIGH);    // allumer la LED rouge
  delay(3000);                  // attendre 3 secondes
  digitalWrite(rouge, LOW);     // éteindre la LED rouge 
  digitalWrite(verte, HIGH);    // allumer la LED verte 
  delay(3000);                  // attendre 3 secondes
  digitalWrite(verte, LOW);     // éteindre la LED verte
}
